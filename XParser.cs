﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using Newtonsoft.Json;

namespace XDiff
{
    class XParser
    {
	    private static bool _setValidation = false;
        private static bool _setNameSpaces = true;
        private static bool _setSchemaSupport = true;
        private static bool _setSchemaFullSupport = false;
        private static bool _setNameSpacePrefixes = true;

        private static int _STACK_SIZE = 100;

        private XTree _xtree;
        private int[] _idStack, _lsidStack; // id and left sibling
        private long[] _valueStack;
        private int _stackTop, _currentNodeID;
        private bool _readElement;
        private StringBuilder _elementBuffer;

        public XParser()
        {
            XHash.initialize();
            _idStack = new int[_STACK_SIZE];
            _lsidStack = new int[_STACK_SIZE];
            _valueStack = new long[_STACK_SIZE];
            _stackTop = 0;
            _currentNodeID = XTree.NULL_NODE;
            _elementBuffer = new StringBuilder();
        }

        void preorder(XNode node)
        {
            XElement ne = node as XElement;
            XText nt = node as XText;
            if (ne != null)
            {
                startElement(ne);
                foreach (XNode child in ne.Nodes())
                    preorder(child);
                endElement(ne);
            }
            if (nt != null)
            {
                String s = nt.Value;
                characters(s.ToCharArray(), 0, s.Length);
            }
        }

        /**
          * Parse an XML document
          * @param  uri input XML document
          * @return the created XTree
          */
        public XTree parse(String uri)
        {
            _xtree = new XTree();
            _idStack[_stackTop] = XTree.NULL_NODE;
            _lsidStack[_stackTop] = XTree.NULL_NODE;

            try
            {
                XDocument xdoc = null;

                if (!File.Exists(uri))
                    throw new Exception("File does not exist.");
                // Test what time of file.
                String ext = Path.GetExtension(uri);
                if (ext.ToLower().Equals(".json"))
                {
                    // Read JSON, then convert to XML.
                    string lines = File.ReadAllText(uri);
                    XmlDocument xmldoc = (XmlDocument)JsonConvert.DeserializeXmlNode(lines);
                    xdoc = XDocument.Parse(xmldoc.OuterXml);
                }
                else if (ext.ToLower().Equals(".xml"))
                {
                    string lines = File.ReadAllText(uri);
                    XmlDocument xmldoc = null;
                    var settings = new XmlReaderSettings();
                    settings.NameTable = new NameTable();
                    var manager = new MyXmlNamespaceManager(settings.NameTable);
                    XmlParserContext context = new XmlParserContext(null, manager, null, XmlSpace.Default);
                    using (var xmlReader = XmlReader.Create(new StringReader(lines), settings, context))
                    {
                        xmldoc = new XmlDocument();
                        xmldoc.Load(xmlReader);
                    }
                    xdoc = XDocument.Parse(xmldoc.OuterXml);
                }

                // Walk tree can convert to XTree data structure.
                XElement x = xdoc.Root;
                preorder(x);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                Environment.Exit(1);
            }

            return _xtree;
        }

        public void startElement(XElement node)
        {
            String uri = "";
            String local = node.Name.LocalName;
            String raw = node.Name.LocalName;

            // if text is mixed with elements
            if (_elementBuffer.Length > 0)
            {
                String text = _elementBuffer.ToString().Trim();
                if (text.Length > 0)
                {
                    long value = XHash.hash(text);
                    int tid = _xtree.addText(_idStack[_stackTop], _lsidStack[_stackTop], text, value);
                    _lsidStack[_stackTop] = tid;
                    _currentNodeID = tid;
                    _valueStack[_stackTop] += value;
                }
            }

            int eid = _xtree.addElement(_idStack[_stackTop],
                _lsidStack[_stackTop], local);

            // Update last sibling info.
            _lsidStack[_stackTop] = eid;

            // Push
            _stackTop++;
            _idStack[_stackTop] = eid;
            _currentNodeID = eid;
            _lsidStack[_stackTop] = XTree.NULL_NODE;
            _valueStack[_stackTop] = XHash.hash(local);

            // Take care of attributes
            foreach (XAttribute attr in node.Attributes())
            {
                String name = attr.Name.LocalName;
                String value = attr.Value.ToString();
                long namehash = XHash.hash(name);
                long valuehash = XHash.hash(value);
                long attrhash = namehash*namehash +
                                valuehash*valuehash;
                int aid = _xtree.addAttribute(eid, _lsidStack[_stackTop], name, value, namehash, attrhash);

                _lsidStack[_stackTop] = aid;
                _currentNodeID = aid + 1;
                _valueStack[_stackTop] += attrhash*attrhash;
            }

            _readElement = true;
            _elementBuffer = new StringBuilder();
        }

        public void characters(char[] ch, int start, int length)
        {
            _elementBuffer.Append(ch, start, length);
        }

        public void endElement(XElement node)
        {
            String uri = "";
            String local = node.Name.LocalName;
            String raw = node.Name.LocalName;

            if (_readElement)
            {
                if (_elementBuffer.Length > 0)
                {
                    String text = _elementBuffer.ToString();
                    long value = XHash.hash(text);
                    _currentNodeID =
                        _xtree.addText(_idStack[_stackTop],
                                   _lsidStack[_stackTop],
                                   text, value);
                    _valueStack[_stackTop] += value;
                }
                else    // an empty element
                {
                    _currentNodeID =
                        _xtree.addText(_idStack[_stackTop],
                                   _lsidStack[_stackTop],
                                   "", 0);
                }
                _readElement = false;
            }
            else
            {
                if (_elementBuffer.Length > 0)
                {
                    String text = _elementBuffer.ToString().Trim();
                    // More text nodes before end of the element.
                    if (text.Length > 0)
                    {
                        long value = XHash.hash(text);
                        _currentNodeID =
                          _xtree.addText(_idStack[_stackTop],
                                 _lsidStack[_stackTop],
                                 text, value);
                        _valueStack[_stackTop] += value;
                    }
                }
            }

            _elementBuffer = new StringBuilder();
            _xtree.addHashValue(_idStack[_stackTop],
                        _valueStack[_stackTop]);
            _valueStack[_stackTop - 1] += _valueStack[_stackTop] *
                            _valueStack[_stackTop];
            _lsidStack[_stackTop - 1] = _idStack[_stackTop];

            // Pop
            _stackTop--;
        }

        public void startCDATA()
        {
            // The text node id should be the one next to the current
            // node id.
            //_xtree.addCDATA(textid, text.length());
        }

        public void endCDATA()
        {
            //_xtree.addCDATA(textid, text.length());
        }
    }
}

