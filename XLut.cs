﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDiff
{
    internal class XLut
    {
        private Dictionary<long, int> _xTable;

        /// <summary>
        /// Constructor.
        /// </summary>
        public XLut()
        {
            _xTable = new Dictionary<long, int>(65536);
        }

        /// <summary>
        /// Add a node pair and their distance to this table.
        /// @param  eid1    element id #1
        /// @param  eid2    element id #2
        /// @param  dist    distance
        /// </summary>
        public virtual void add(int eid1, int eid2, int dist)
        {
            System.Console.WriteLine("distance between "
                + eid1 + " " + eid2 + " = " + dist);
            long key = eid1;
            key = key << 32;
            key += eid2;

            _xTable[key] = dist;
        }

        /// <summary>
        /// Get the distance of a node pair.
        /// @param  eid1    element id #1
        /// @param  eid2    element id #2
        /// @return distance or -1 if not found
        /// </summary>
        public virtual int get(int eid1, int eid2)
        {
            long key = eid1;
            key = key << 32;
            key += eid2;

            if (!_xTable.ContainsKey(key))
            {
                return XTree.NO_CONNECTION;
            }
            else
            {
                return _xTable[key];
            }
        }
    }
}
