﻿using System.Collections.Generic;
using System.Data.Odbc;
using System.Xml;


namespace XDiff
{
    class MyXmlNamespaceManager : XmlNamespaceManager
    {
        const string DefaultMissingNamespacePrefix = "http://missing.namespace.prefix.net/2014/";

        private string MissingNamespacePrefix { get; set; }
        private int NextMissingNamespaceIndex { get; set; }

        // The dictionary consists of a collection of namespace names keyed by prefix.
        public Dictionary<string, List<string>> MissingNamespaces { get; private set; }

        public MyXmlNamespaceManager(XmlNameTable nameTable)
            : this(nameTable, null)
        { }

        public MyXmlNamespaceManager(XmlNameTable nameTable, string missingNamespacePrefix)
            : base(nameTable)
        {
            this.MissingNamespacePrefix = (string.IsNullOrEmpty(missingNamespacePrefix) ? DefaultMissingNamespacePrefix : missingNamespacePrefix);
            this.MissingNamespaces = new Dictionary<string, List<string>>();
        }

        void AddMissingNamespace(string prefix)
        {
            if (string.IsNullOrEmpty(prefix))
                return;

            string uri;
            do
            {
                int index = NextMissingNamespaceIndex++;
                uri = MissingNamespacePrefix + index.ToString();
            } while (LookupPrefix(uri) != null); // Just in case.

            AddNamespace(prefix, uri);

            List<string> r = null;
            if (MissingNamespaces.ContainsKey(prefix))
            {
                r = MissingNamespaces[prefix];
                if (!r.Contains(uri))
                    r.Add(uri);
            }
            else
            { 
                r = new List<string>();
                r.Add(uri);
                MissingNamespaces.Add(prefix, r);
            }
        }

        public override bool HasNamespace(string prefix)
        {
            var result = base.HasNamespace(prefix);
            if (!result)
                AddMissingNamespace(prefix);
            result = base.HasNamespace(prefix);
            return result;
        }

        public override string LookupNamespace(string prefix)
        {
            var result = base.LookupNamespace(prefix);
            if (result == null)
                AddMissingNamespace(prefix);
            result = base.LookupNamespace(prefix);
            return result;
        }
    }
}
