﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDiff
{
    internal class XTree
    {
        public const int MATCH = 0;
        public const int CHANGE = 1;
        public const int NO_MATCH = -1;
        public const int INSERT = -1;
        public const int DELETE = -1;
        public const int NULL_NODE = -1;
        public const int NO_CONNECTION = 1048576;

        private static int _TOP_LEVEL_CAPACITY = 16384;
        private static int _BOT_LEVEL_CAPACITY = 4096;
        private static int _root = 0;

        // Many arrays in this class are indexed by the pre-order
        // number of a node. However, rather than encode the tree in
        // 1-dimensional arrays, the author uses a 2-dimensional array
        // in row-major order. _topCap encodes the number of rows;
        // _botCap encodes the number of columns.
        private readonly int _topCap, _botCap;

        private int _elementIndex, _tagIndex, _valueCount;

        // Each node in the tree has an id. The id ranges from 0 to some positive
        // number. The id in the tree is the pre-order traversal for the node.
        // _firstChild is the left most immediate child of a given node, indexed
        // in row-major order. If there is no child, then the _firstChild[] entry will
        // be -1. In fact, alternatively one could infer a node is frontier if the next
        // entry in the array is the same--it is impossible to reuse a node id in a tree.
        private int[][] _firstChild;

        // _nextSibling is an array indexed by node id (row-major format) that
        // indicates what the next sibling is. If it's -1, then there is no sibling
        // following the node.
        private int[][] _nextSibling;

        // _childrenCount is the number of children for a node,
        // 0 if it is a leaf node.
        private int[][] _childrenCount;

        // _valueIndex is a mapping of the node to the string value of the node.
        // _tagNames is a table of those strings, mapping the string into a unique number.
        // That number is what is stored in _valueIndex.
        private int[][] _valueIndex;

        // _isAttribute notes whether the leaf node is an attribute for the element (parent).
        private bool[][] _isAttribute;

        private int[][] _matching;

        private long[][] _hashValue;

        private string[][] _value;

        private Dictionary<String, int> _tagNames;

        private Dictionary<int, List<int>> _cdataTable;



        private Dictionary<int, ulong> isolated_hash_value = new Dictionary<int, ulong>();
        private Dictionary<int, ulong> computed_hash_value = new Dictionary<int, ulong>();
        private Dictionary<int, String> _kenstagNames = new Dictionary<int, String>();


        public void Dump(int level, int id)
        {
            int topid = id / _botCap;
            int botid = id % _botCap;
            int vi = this._valueIndex[topid][botid];
            String v = this._tagNames.FirstOrDefault(x => x.Value == vi).Key;
            long hash = this.getHashValue(id);
            System.Console.Write(new String(' ', level*3));
            System.Console.Write(v + " " + id + " ");
            System.Console.Write(" iso {0:X}", isolated_hash_value[id]);
            System.Console.Write(" comp {0:X}", hash);
            System.Console.WriteLine();
            int c = this._firstChild[topid][botid];
            while (c != NULL_NODE)
            {
                Dump(level + 1, c);
                c = this._nextSibling[c/_botCap][c%_botCap];
            }
        }

        public void BottomUpHashCompute(int id)
        {
            int topid = id / _botCap;
            int botid = id % _botCap;
            ulong iso = this.isolated_hash_value[id];
            int vi = this._valueIndex[topid][botid];
            String v = this._tagNames.FirstOrDefault(x => x.Value == vi).Key;
            bool is_text = false;
            if (v == null)
            {
                v = this._kenstagNames[id];
                is_text = true;
            }
            ulong iso2 = (ulong)XHash.hash(v);
            if (iso != iso2)
                throw new Exception("fuck!");
            this.computed_hash_value[id] = 0;
            int c = this._firstChild[topid][botid];
            bool first = true;
            Stack<int> stack = new Stack<int>();
            int count = 0;
            while (c != NULL_NODE)
            {
                count++;
                c = this._nextSibling[c/_botCap][c%_botCap];
            }

            c = this._firstChild[topid][botid];
            while (c != NULL_NODE)
            {
                BottomUpHashCompute(c);
                if (count > 1)
                    computed_hash_value[id] += computed_hash_value[c] * computed_hash_value[c];
                else
                    computed_hash_value[id] += computed_hash_value[c];
                c = this._nextSibling[c / _botCap][c % _botCap];
            }
            computed_hash_value[id] += iso;
        }


        /**
          * Default constructor
          */
        public XTree()
        {
            _topCap = _TOP_LEVEL_CAPACITY;
            _botCap = _BOT_LEVEL_CAPACITY;
            _initialize();
        }

        /**
          * Constructor that allows users to modify settings.
          */
        public XTree(int topcap, int botcap)
        {
            _topCap = topcap;
            _botCap = botcap;
            _initialize();
        }

        // Initialization.
        private void _initialize()
        {
            _firstChild = new int[_topCap][];
            _nextSibling = new int[_topCap][];
            _isAttribute = new bool[_topCap][];
            _valueIndex = new int[_topCap][];
            _matching = new int[_topCap][];
            _childrenCount = new int[_topCap][];
            _hashValue = new long[_topCap][];
            _value = new string[_topCap][];

            _value[0] = new string[_botCap];
            _tagNames = new Dictionary<String, int>(_botCap);

            // This hashtable is used to record CDATA section info.
            // The key is the text node id, the value is the list of 
            // (start,end) position pair of each CDATA section.
            _cdataTable = new Dictionary<int, List<int>>(_botCap);

            _elementIndex = -1;
            _tagIndex = -1;
            _valueCount = _botCap - 1;
        }

        /**
          * ID Expansion
          */
        private void _expand(int topid)
        {
            _firstChild[topid] = new int[_botCap];
            _nextSibling[topid] = new int[_botCap];
            _childrenCount[topid] = new int[_botCap];
            _matching[topid] = new int[_botCap];
            _valueIndex[topid] = new int[_botCap];
            _hashValue[topid] = new long[_botCap];
            _isAttribute[topid] = new bool[_botCap];

            for (int i = 0; i < _botCap; i++)
            {
                _firstChild[topid][i] = NULL_NODE;
                _nextSibling[topid][i] = NULL_NODE;
                _childrenCount[topid][i] = 0;
                _matching[topid][i] = MATCH;
                _valueIndex[topid][i] = -1;
                _isAttribute[topid][i] = false;
            }
        }

        // Start  -- methods for constructing a tree.
        /**
          * Add a new element to the tree.
          * @param	pid		parent id
          * @param	lsid		left-side sibling id
          * @param	tagName		element name
          * @return	the element id in the tree.
          */
        public int addElement(int pid, int lsid, String tagName)
        {
            _elementIndex++;

            isolated_hash_value[_elementIndex] = (ulong)XHash.hash(tagName);

            int topid = _elementIndex / _botCap;
            int botid = _elementIndex % _botCap;
            if (botid == 0)
                _expand(topid);

            // Check if we've already had the tag
            int tagID = 0;
            if (_tagNames.ContainsKey(tagName))
            {
                tagID = _tagNames[tagName];
                _valueIndex[topid][botid] = tagID;
            }
            else
            {
                _tagIndex++;
                tagID = _tagIndex;
                _value[0][_tagIndex] = tagName;
                _tagNames[tagName] = tagID;
                _valueIndex[topid][botid] = _tagIndex;
            }

            if (pid == NULL_NODE)
                return _elementIndex;

            int ptopid = pid / _botCap;
            int pbotid = pid % _botCap;
            // parent-child relation or sibling-sibling relation
            if (lsid == NULL_NODE)
                _firstChild[ptopid][pbotid] = _elementIndex;
            else
                _nextSibling[lsid / _botCap][lsid % _botCap] = _elementIndex;

            // update children count
            _childrenCount[ptopid][pbotid]++;

            System.Console.WriteLine("count[" + ptopid + "," + pbotid + "] = " +
                _childrenCount[ptopid][pbotid]);

            return _elementIndex;
        }

        /**
          * Add a text node.
          * @param	eid	element id
          * @param	lsid	the sibling id on the left
          * @param	text	text value
          * @param	value	hash value
          */
        public int addText(int eid, int lsid, String text, long value)
        {
            _elementIndex++;

            isolated_hash_value[_elementIndex] = (ulong)XHash.hash(text);
            _kenstagNames[_elementIndex] = text;

            int topid = _elementIndex / _botCap;
            int botid = _elementIndex % _botCap;
            if (botid == 0)
                _expand(topid);

            int etopid = eid / _botCap;
            int ebotid = eid % _botCap;
            if (lsid == NULL_NODE)
                _firstChild[etopid][ebotid] = _elementIndex;
            else
                _nextSibling[lsid / _botCap][lsid % _botCap] = _elementIndex;

            _childrenCount[etopid][ebotid]++;
            _hashValue[topid][botid] = value;

            _valueCount++;
            int vtopid = _valueCount / _botCap;
            int vbotid = _valueCount % _botCap;
            if (vbotid == 0)
                _value[vtopid] = new String[_botCap];

            _value[vtopid][vbotid] = text;
            _valueIndex[topid][botid] = _valueCount;

            return _elementIndex;
        }

        /**
          * Add an attribute.
          * @param	eid	element id
          * @param	lsid	the sibling id on the left
          * @param	name	attribute name
          * @param	value	attribute value
          * @param	valuehash	hash value of the value
          * @param	attrhash	hash value of the entire attribute
          * @return	the element id of the attribute
          */
        public int addAttribute(int eid, int lsid, String name, String value,
                    long valuehash, long attrhash)
        {
            // attribute name first.
            int aid = addElement(eid, lsid, name);

            // attribute value second.
            addText(aid, NULL_NODE, value, valuehash);

            // hash value third
            int atopid = aid / _botCap;
            int abotid = aid % _botCap;
            _isAttribute[atopid][abotid] = true;
            _hashValue[atopid][abotid] = attrhash;

            return aid;
        }

        /**
          * Add more information (hash value) to an element node.
          * @param	eid	element id
          * @param	value	extra hash value
          */
        public void addHashValue(int eid, long value)
        {
            _hashValue[eid / _botCap][eid % _botCap] = value;
        }

        /**
          * Add a CDATA section (either a start or an end) to the CDATA
          * hashtable, in which each entry should have an even number of
          * position slots.
          * @param	eid		The text node id
          * @param	position	the section tag position
          */
        public void addCDATA(int eid, int position)
        {
            int key = eid;
            List<int> value = null;//_cdataTable.get(key);
            if (!_cdataTable.ContainsKey(key))
            {
                List<int> list = new List<int>();
                list.Add(position);
                _cdataTable[key] = list;
            }
            else
            {
                value = _cdataTable[key];
                List<int> list = value;
                list.Add(position);
                _cdataTable[key] = list;
            }
        }

        /**
          * Add matching information.
          * @param	eid	element id
          * @param	match	?match and matched element id
          */
        public void addMatching(int eid, int[] match)
        {
            if (match[0] == NO_MATCH)
                _matching[eid / _botCap][eid % _botCap] = NO_MATCH;
            else if (match[0] == MATCH)
                _matching[eid / _botCap][eid % _botCap] = MATCH;
            else
                _matching[eid / _botCap][eid % _botCap] = match[1] + 1;
            System.Console.Write("Add " + eid);
            if (match[0] == NO_MATCH) System.Console.Write(" No Match ");
            else if (match[0] == MATCH) System.Console.Write(" Match ");
            else System.Console.Write(" " + (match[1]));
            System.Console.WriteLine();
        }

        // End  -- methods for constructing a tree.

        // Start -- methods for accessing a tree.

        /**
          * Get matching information.
          * @param	eid	element id
          * @param	match	?change and matched element id 
          */
        public void getMatching(int eid, int[] match)
        {
            int mid = _matching[eid / _botCap][eid % _botCap];
            if (mid == NO_MATCH)
                match[0] = NO_MATCH;
            else if (mid == MATCH)
                match[0] = MATCH;
            else
            {
                match[0] = CHANGE;
                match[1] = mid - 1;
            }
        }

        /**
          * Get the root element id.
          */
        public int getRoot()
        {
            return _root;
        }

        /**
          * Get the first child of a node.
          * @param	eid	element id
          */
        public int getFirstChild(int eid)
        {
            int cid = _firstChild[eid / _botCap][eid % _botCap];
            while (cid > _root)
            {
                int ctopid = cid / _botCap;
                int cbotid = cid % _botCap;
                if (_isAttribute[ctopid][cbotid])
                    cid = _nextSibling[ctopid][cbotid];
                else
                    return cid;
            }

            return NULL_NODE;
        }

        /**
          * Get the next sibling of a node.
          * @param	eid	element id
          */
        public int getNextSibling(int eid)
        {
            return _nextSibling[eid / _botCap][eid % _botCap];
        }

        /**
          * Get the first attribute of a node.
          * @param	eid	element id
          */
        public int getFirstAttribute(int eid)
        {
            int aid = _firstChild[eid / _botCap][eid % _botCap];
            if ((aid > _root) && (_isAttribute[aid / _botCap][aid % _botCap]))
                return aid;
            else
                return NULL_NODE;
        }

        /**
          * Get the next attribute of a node.
          * @param	aid	attribute id
          */
        public int getNextAttribute(int aid)
        {
            int aid1 = _nextSibling[aid / _botCap][aid % _botCap];
            if ((aid1 > _root) && (_isAttribute[aid1 / _botCap][aid1 % _botCap]))
                return aid1;
            else
                return NULL_NODE;
        }

        /**
          * Get the attribute value.
          * @param	aid	attribute id
          */
        public String getAttributeValue(int aid)
        {
            int cid = _firstChild[aid / _botCap][aid % _botCap];
            int index = _valueIndex[cid / _botCap][cid % _botCap];
            if (index > 0)
                return _value[index / _botCap][index % _botCap];
            else
                return "";
        }

        /**
          * Get the hash value of a node.
          * @param	eid	element id
          */
        public long getHashValue(int eid)
        {
            return _hashValue[eid / _botCap][eid % _botCap];
        }

        /**
          * Get the CDATA section position list of a text node.
          * @param	eid	element id
          * @return	position list which is a vector or null if no CDATA
          */
        public List<int> getCDATA(int eid)
        {
            if (_cdataTable.ContainsKey(eid))
                return _cdataTable[eid];
            else return null;
        }

        /**
          * Get the childern count of a node.
          * @param	eid	element id
          */
        public int getChildrenCount(int eid)
        {
            return _childrenCount[eid / _botCap][eid % _botCap];
        }

        /**
          * Get the # of all decendents of a node.
          * @param	eid	element id
          */
        public int getDecendentsCount(int eid)
        {
            int topid = eid / _botCap;
            int botid = eid % _botCap;
            int count = _childrenCount[topid][botid];
            if (count == 0)
                return 0;

            int cid = _firstChild[topid][botid];
            while (cid > NULL_NODE)
            {
                count += getDecendentsCount(cid);
                cid = _nextSibling[cid / _botCap][cid % _botCap];
            }

            return count;
        }

        /**
          * Get the value index of a node
          * @param	eid	element id
          */
        public int getValueIndex(int eid)
        {
            return _valueIndex[eid / _botCap][eid % _botCap];
        }

        /**
          * Get the value of a leaf node
          * @param	index	value index
          */
        public String getValue(int index)
        {
            return _value[index / _botCap][index % _botCap];
        }

        /**
          * Get the tag of an element node
          * @param	eid	element id
          */
        public String getTag(int eid)
        {
            int index = _valueIndex[eid / _botCap][eid % _botCap];
            return _value[0][index];
        }

        /**
          * Get the text value of a leaf node
          * @param	eid	element id
          */
        public String getText(int eid)
        {
            int index = _valueIndex[eid / _botCap][eid % _botCap];
            if (index >= _botCap)
                return _value[index / _botCap][index % _botCap];
            else
                return "";
        }

        /**
          * Check if a node an element node.
          * @param	eid	element id
          */
        public bool isElement(int eid)
        {
            int vindex = _valueIndex[eid / _botCap][eid % _botCap];
            if (vindex < _botCap)
                return true;
            else
                return false;
        }

        /**
          * Check if a node is an attribute node.
          * @param	eid	element id
          */
        public bool isAttribute(int eid)
        {
            return _isAttribute[eid / _botCap][eid % _botCap];
        }

        /**
          * Check if a node an leaf text node.
          * @param	edi	element id
          */
        public bool isLeaf(int eid)
        {
            int index = _valueIndex[eid / _botCap][eid % _botCap];
            if (index < _botCap)
                return false;
            else
                return true;
        }

        // End  -- methods for accessing a tree.

        /**
          * For testing purpose.
          */
        public void dump()
        {
            System.Console.WriteLine("eid\tfirstC\tnextS\tattr?\tcCount\thash\tmatch\tvalue\n");
            for (int i = _root; i <= _elementIndex; i++)
            {
                int topid = i / _botCap;
                int botid = i % _botCap;
                int vid = _valueIndex[topid][botid];
                int vtopid = vid / _botCap;
                int vbotid = vid % _botCap;
                System.Console.WriteLine(i + "\t" +
                           _firstChild[topid][botid] + "\t" +
                           _nextSibling[topid][botid] + "\t" +
                           _isAttribute[topid][botid] + "\t" +
                           _childrenCount[topid][botid] + "\t" +
                           _hashValue[topid][botid] + "\t" +
                           _matching[topid][botid] + "\t" +
                           _value[vtopid][vbotid]);
            }
        }
        public void dump(int eid)
        {
            int topid = eid / _botCap;
            int botid = eid % _botCap;
            int vid = _valueIndex[topid][botid];
            int vtopid = vid / _botCap;
            int vbotid = vid % _botCap;
            System.Console.WriteLine(eid + "\t" +
                       _firstChild[topid][botid] + "\t" +
                       _nextSibling[topid][botid] + "\t" +
                       _isAttribute[topid][botid] + "\t" +
                       _childrenCount[topid][botid] + "\t" +
                       _hashValue[topid][botid] + "\t" +
                       _matching[topid][botid] + "\t" +
                       _value[vtopid][vbotid]);
        }
    }
}
